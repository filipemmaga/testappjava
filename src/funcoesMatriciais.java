public class funcoesMatriciais {

  public static void main(String[] args) {
   /*
   f(x,y) = (3*(x+1) + (y*y));
   (0 < x < 9) e (0 < y < 9)
    */

    int[][] list = new int[10][10];

    for (int x = 0; x < 9; x++) {
      String str = "";
      for (int y = 0; y < 9; y++) {
        list[x][y] = (3 * (x + 1) + (y * y));

        if (y < 9)
          str += list[x][y] + ", ";
        else
          str += list[x][y];

      }
      System.out.println(str);
    }
  }
}
