public class ManipulaArray {
    public static void main(String[] args){
        int[] array = new int[10];
        array[0]= 10;
        array[1]=20;
        array[2]=30;
        array[3]=30;
        array[4]=30;
        array[5]=30;
        array[6]=30;
        array[7]=30;
        array[8]=30;
        array[9]=30;

        int soma = 0;
        for(int i = 0; i < array.length; i++){
            soma += array[i];
            System.out.println("O valor do Array:" + soma);
        }
        System.out.println("O valor Total do Array:" + soma);

    }
}
