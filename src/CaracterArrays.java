public class CaracterArrays {
    public static void main(String[] args){
        int[] array = new int[10];

        array[0]= 0;
        array[1]= 11;
        array[2]= 22;
        array[3]= 33;
        array[4]= 44;
        array[6]= 66;
        array[7]= 77;
        array[8]= 88;
        array[5] = 1000;

        System.out.println(array[4]);
        System.out.println();
        System.out.println(array[5]);
        System.out.println();
        System.out.println(array[9]);
    }
}
