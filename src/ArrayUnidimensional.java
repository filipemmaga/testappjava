public class ArrayUnidimensional {
    public static void main(String[] args){
        int[] array = {78, 80, 20, 30, 100};
        System.out.printf("%s%8s\n", "Indice", "Valores");
        for (int i = 0; i < array.length; i++){
            System.out.printf("%5d%8d\n", i, array[i]);
        }
    }
}
